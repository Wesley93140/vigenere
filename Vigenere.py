def vigenere(texte, cle, mode=False):
    Letter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    sentence = texte.upper().replace(" ", "")
    key = cle.upper()
    keyLen = len(key)
    keyShift = [Letter.find(i) for i in key]
    sentenceShift = [Letter.find(i) for i in sentence]
    result = ""

    for i in range(len(sentence)):
        if mode == False:
            shift = (sentenceShift[i] + keyShift[i % keyLen]) % 26   # Calculer le décalage
        else:
            shift = (sentenceShift[i] - keyShift[i % keyLen]) % 26   # Calculer le décalage
        result+= Letter[shift]
    return result
print("vig : ", vigenere("CAVAETRETOUTNOIR", "PIERRE"))
