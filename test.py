# coding: utf-8
from Vigenere import *
Letter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def testEncrypt():
    sentence = "CAVAETRETOUTNOIR"
    key = "PIERRE"
    if (vigenere(sentence, key) == "RI ZR VXGM XFLX CWMI"):
        return True
    else:
        return False
    
def testDecrypt():
    if (vigenere("RI ZR VXGM XFLX CWMI", "PIERRE", mode=True) == "CA VA ETRE TOUT NOIR"):
        return True
    else:
        return False
